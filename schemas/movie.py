from pydantic import BaseModel, Field
from typing import Optional, List

class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_Length=5, max_Length=15)
    overview: str = Field(min_Length=15, max_Length=50)
    year: int = Field(ge=1950, le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_Length=5, max_Length=15)

    class Config:
        json_schema_extra = {
            "example": {
                "id": 1,
                "title": "Mi Película",
                "overview": "Descripción de la película",
                "year": 2024,
                "rating": 9.8,
                "category": "Acción"
            }
        }
